package core;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class DerbiParser {
	public static ObjectMapper mapper = new ObjectMapper();
	
	public static <T> T jsonToObject(JsonNode json, Class<T> classOfObject){
		try {
			return (T)mapper.readValue(json.toString(), classOfObject);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static <T> T jsonToObject(String json, Class<T> classOfObject){
		try {
			return (T)mapper.readValue(json, classOfObject);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static <T> JsonNode ObjectToJson(T model){
		try {
			return mapper.readTree(mapper.writeValueAsString(model));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	public static DBObject JsonNodeToDBObject(JsonNode json){
		return  (DBObject)JSON.parse(json.toString());
	}
	
	
	public static JsonNode DBObjectTOJsonNode(DBObject json){
		try {
			return  mapper.readTree(json.toString());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static <T> List<T> jsonListToObjectList(String jsonLst, Class<?> lstClass){
		try {
			return mapper.readValue(jsonLst, mapper.getTypeFactory().constructCollectionType(List.class, lstClass));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
}
