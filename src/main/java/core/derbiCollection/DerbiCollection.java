package core.derbiCollection;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import core.DatabaseService;
import core.interfaces.IntependentObjects;
import models.Catagory;
import models.Product;

public class DerbiCollection{
	public enum DerbiCollections implements IntependentObjects{
		DERBI_PRODUCTS("derbi-products",Product.class),
		DERBI_CATEGORIES("derbi-categories",Catagory.class);
		
		
		private DBCollection col;
		private Integer seq;
		private Class<?> model;
		private DerbiCollections(String collectionName, Class<?> model){
			this.model = model;
			col = DatabaseService.DBType.DB.getDB().getCollection(collectionName);
			seq = null;
			
		}
	
		public DBCollection collection() {
			return col;
		}
	
		public long getSequensce() {
			if(seq == null){
				DBObject seqJson = new BasicDBObject();
				seqJson.put("_id", "sequence_id");
				seqJson.put("counter", 1);
				col.insert(seqJson);
				seq = 1;
			}
			return seq;
		}
	
		public long getAndIncrementSequensce() {
			if(seq == null){
				DBObject seqJson = new BasicDBObject();
				seqJson.put("_id", "sequence_id");
				seqJson.put("counter", 1);
				col.insert(seqJson);
				seq = 1;
			}
			DBObject updateQuery = new BasicDBObject();
			updateQuery.put("_id", "sequence_id");
			DBObject preformeUpadete = new BasicDBObject();
			preformeUpadete.put("$inc", new BasicDBObject("counter",1));
			col.update(updateQuery, preformeUpadete);
			return ++seq;
			
		}
		
		public Class<?> getModelClass(){
			return model;
		}
	
	}
	
	public static void initDerbiCollection(){
		for(IntependentObjects cur: DerbiCollections.values())
			DatabaseService.addModel(cur.getClass(),cur.collection());
	}
	
	
}