package core;

import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

import core.interfaces.IndependentObject;

public class DatabaseService {
	private static HashMap<Class<?>, DBCollection> map = new HashMap<Class<?>, DBCollection>();
	
	public enum DBType{
		DB();
		private DB conection;
		@SuppressWarnings("deprecation")
		private DBType() {
			conection = new DB(new Mongo("localhost"), "derbi");
		}
		public DB getDB(){
			return conection;
		}
		
	}
	
	public static void addModel(Class<?> model, DBCollection col){
		map.put(model, col);
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends IndependentObject<T>> T getFrobDBByUniqueId(T model){
		JsonNode query = model.getUniuqId();
		DBCursor resultCurser = map.get(model.getClass()).find(DerbiParser.JsonNodeToDBObject(query));
		return (T)DerbiParser.jsonToObject(resultCurser.toArray().iterator().next().toString(), model.getClass());
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends IndependentObject<T>> List<T> read(T model , JsonNode conditions){
		JsonNode modelJson = model.toJson();
		((ObjectNode)modelJson).setAll((ObjectNode)conditions);
		DBCursor resultCurser = map.get(model.getClass()).find(DerbiParser.JsonNodeToDBObject(modelJson));
		return ((List<T>)DerbiParser.jsonListToObjectList(resultCurser.toArray().toString(),model.getClass()));
	}
	
	
	public static <T extends IndependentObject<T>> List<T> read(T model){
		return read(model,new JsonNodeFactory(false).objectNode());
	}
	
	public static <T extends IndependentObject<T>> boolean delete(T model, JsonNode conditions){
		JsonNode modelJson = model.toJson();
		((ObjectNode)modelJson).setAll((ObjectNode)conditions);
		return map.get(model.getClass()).remove(DerbiParser.JsonNodeToDBObject(modelJson)).getN()>0;
	}
	
	
	public static <T extends IndependentObject<T>> boolean delete(T model){
		return delete(model,new JsonNodeFactory(false).objectNode());
	}
	
	
	public static <T extends IndependentObject<T>> boolean create(T model){
		JsonNode modelJson = model.toJson();
		return map.get(model.getClass()).insert(DerbiParser.JsonNodeToDBObject(modelJson)).getN()>0;
	}
	
	
	public static <T extends IndependentObject<T>> boolean update(T model,JsonNode conditions){
		JsonNode modelJson = model.toJson();
		JsonNode query = model.getUniuqId();
		((ObjectNode)query).setAll((ObjectNode)conditions);
		DBObject up = new BasicDBObject();
		up.put("$set", DerbiParser.JsonNodeToDBObject(modelJson));
		return map.get(model.getClass()).update(DerbiParser.JsonNodeToDBObject(query),up ).getN()>0;
	}
	
	public static <T extends IndependentObject<T>> boolean update(T model){
		return update(model,new JsonNodeFactory(false).objectNode());
	}
	
	
	
}
