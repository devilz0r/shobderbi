package core.interfaces;

import com.mongodb.DBCollection;

public interface IntependentObjects {
	public DBCollection collection();
	public long getSequensce();
	public long getAndIncrementSequensce();
	public Class<?> getModelClass();
}
