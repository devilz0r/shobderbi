package core.interfaces;

import com.fasterxml.jackson.databind.JsonNode;

public interface BaseModel<T> {
	public T duplicate();
	public JsonNode toJson();
}
