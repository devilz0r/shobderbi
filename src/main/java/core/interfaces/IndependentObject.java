package core.interfaces;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

public interface IndependentObject<T> extends IdentifiedObject<T>{
	public List<T> read();
	public List<T> read(JsonNode conditions);
	public T getFrobDBByUniqueId();
	public boolean delete();
	public boolean delete(JsonNode conditions);
	public boolean update();
	public boolean update(JsonNode conditions);
	public boolean create();
}
