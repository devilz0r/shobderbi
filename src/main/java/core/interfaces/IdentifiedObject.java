package core.interfaces;

import com.fasterxml.jackson.databind.JsonNode;

public interface IdentifiedObject<T> extends BaseModel<T>{
	public JsonNode getUniuqId();
}
