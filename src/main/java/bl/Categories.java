package bl;

import java.util.List;

import core.derbiCollection.DerbiCollection.DerbiCollections;
import models.Catagory;
import models.Product;

public class Categories {
	public static List<Catagory> getAllCategories(){
		return new Catagory().read();
	}
	
	public static boolean addNewCategory(String category, String description, String image){
		//TODO: validate premission
		//TODO: IMage upload
		return new Catagory().setId(DerbiCollections.DERBI_CATEGORIES.getAndIncrementSequensce()).setCategory(category).setDescription(description).setImage(image).create();
	}
	
	public static boolean deleteCategory(Long id){
		//TODO: check Premission 
		Catagory toDelete = new Catagory().setId(id).getFrobDBByUniqueId();
		Products.getProductsFilterd((double)0,(double) 0, toDelete.getCatagory(), null).stream().map((cur)->cur.delete());
		return toDelete.delete();
	}
	
	public static boolean updateCategory(Long id, String category,  String description, String image){
		//TODO: image upload
		Catagory old = new Catagory().setId(id).getFrobDBByUniqueId();
		new Product().setCatagory(old.getCatagory()).read().stream().map(cur->cur.setCatagory(category).update());
		return old.setCategory(category).setDescription(description).setImage(image).update();
	}
	
}
