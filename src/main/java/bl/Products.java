package bl;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import core.DerbiParser;
import core.derbiCollection.DerbiCollection.DerbiCollections;
import models.Product;

public class Products {
	
	public static List<Product> getAllVisableProducts(){
		return new Product().setVisable(true).read();
	}
	
	
	@SuppressWarnings("deprecation")
	public static List<Product> getProductsFilterdForClient(Double fromPrice, Double toPrice, String category, String manyfacturer){
		Product toRead = new Product();
		if(!category.equals("") && category != null) toRead.setCatagory(category);
		if(!manyfacturer.equals("") && manyfacturer != null) toRead.setManyfacture(manyfacturer);
		ObjectNode constreins = JsonNodeFactory.instance.objectNode();
		if(toPrice != 0)
			constreins.put("$lte", JsonNodeFactory.instance.objectNode().put("price", toPrice));
		if(fromPrice != 0)
			constreins.put("$gte", JsonNodeFactory.instance.objectNode().put("price", fromPrice));
		return toRead.setVisable(true).read((JsonNode)constreins);
	}
	
	
	@SuppressWarnings("deprecation")
	public static List<Product> getProductsFilterd(Double fromPrice, Double toPrice, String category, String manyfacturer){
		// TODO: PremmisionCheack
		Product toRead = new Product();
		if(!category.equals("") && category != null) toRead.setCatagory(category);
		if(!manyfacturer.equals("") && manyfacturer != null) toRead.setManyfacture(manyfacturer);
		ObjectNode constreins = JsonNodeFactory.instance.objectNode();
		if(toPrice != 0)
			constreins.put("$lte", JsonNodeFactory.instance.objectNode().put("price", toPrice));
		if(fromPrice != 0)
			constreins.put("$gte", JsonNodeFactory.instance.objectNode().put("price", fromPrice));
		return toRead.read((JsonNode)constreins);
		
	}
	
	
	
	public static List<Product> getAllProducts(){
		//TODO: Premission Chack
		
		
		return new Product().read();
	}
	
	
	public static boolean createNewProduct(JsonNode product){
		//TODO: add Premission chack
		
		Product newprod = DerbiParser.jsonToObject(product, Product.class);
		return newprod.setId(DerbiCollections.DERBI_PRODUCTS.getAndIncrementSequensce()).create();
	}
	
	public static boolean updateProduct(JsonNode product){
		//TODO: add Premission chack
		
		Product newprod = DerbiParser.jsonToObject(product, Product.class);
		return newprod.update();
	}
	
	
	public static boolean DeleteProduct(Long id){
		//TODO: add Premission chack
		
		return new Product().setId(id).delete();
	}
	
}
