package models;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;

import core.interfaces.IndependentObject;

public class Product extends BaseModel<Product> implements IndependentObject<Product>{
	private Long id;
	private String productName;
	private String displayDescription;
	private String description;
	private Double price;
	private String imageName;
	private String pdfName;
	private String manyfacture;
	private String type;
	private String catagory;
	private Boolean visable;
	
	public Product(Long id, String productName, String displayDescription,String description,Double price, String imageName, String pdfName, String manyfacture, String catagory, Boolean visable){
		this.type = "product";
		this.id = id;
		this.description = description;
		this.displayDescription = displayDescription;
		this.imageName = imageName;
		this.pdfName = pdfName;
		this.productName = productName;
		this.price = price;
		this.manyfacture = manyfacture;
		this.catagory = catagory;
		this.visable = visable;
	}
	
	public Product(){
		this.type = "product";
	}
	
	public Product setId(Long id){this.id = id;	return this;}
	
	public Product setDescription(String description){this.description = description;	return this;}
	
	public Product setDisplayDescription(String displayDescription){this.displayDescription = displayDescription;	return this;}
	
	public Product setImageName(String imageName){this.imageName = imageName;	return this;}
	
	public Product setPdfName(String pdfName){this.pdfName = pdfName;	return this;}
	
	public Product setProductName(String productName){this.productName = productName;	return this;}
	
	public Product setPrice(Double price){this.price = price;	return this;}
	
	public Product setManyfacture(String manyfacture){this.manyfacture = manyfacture;	return this;}
	
	public Product setCatagory(String catagory){this.catagory = catagory;	return this;}
	
	public Product setVisable(Boolean visable){this.visable = visable; return this;}
	
	public Long getId(){return id;}
	
	public String getDescription(){return description;}
	
	public String getDisplayDescription(){return displayDescription;}
	
	public String getImageName(){return imageName;}
	
	public String getPdfName(){return pdfName;}
	
	public String getProductName(){return productName;}
	
	public Double getPrice(){return price;}
	
	public String getManyfacture(){return manyfacture;}
	
	public String getType(){return type;}
	
	public String getCatagory(){return catagory;}
	
	public Boolean getVisable(){return visable;}
	
	public JsonNode getUniuqId() {
		return (JsonNode)JsonNodeFactory.instance.objectNode().put("id", id);
	}

}