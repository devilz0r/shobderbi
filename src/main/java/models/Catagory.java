package models;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;

public class Catagory extends BaseModel<Catagory>{

	private String category;
	private String description;
	private String image;
	private Long id;
	
	public Catagory(){}
	
	
	public String getCatagory(){return category;}
	
	public String getDescription(){return description;}
	
	public String getImage(){return image;}
	
	public Long getId(){return id;}
	
	
	public Catagory setCategory(String category){this.category = category; return this;}
	
	public Catagory setDescription(String description){this.description = description; return this;}
	
	public Catagory setImage(String image){this.image = image; return this;}
	
	public Catagory setId(Long id){this.id = id; return this;}
	
	@Override
	public JsonNode getUniuqId() {
		return (JsonNode)JsonNodeFactory.instance.objectNode().put("id", id);
	}

}
