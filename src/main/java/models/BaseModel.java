package models;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

import core.DatabaseService;
import core.DerbiParser;
import core.interfaces.IndependentObject;

@SuppressWarnings("unchecked")
public abstract class BaseModel <T extends IndependentObject<T>> implements IndependentObject<T>{
	
	
	public T duplicate() {
		try {
			return ((T) ((T)this).getClass().getConstructor(((T)this).getClass()).newInstance(this));
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			return null;
		}
	}

	public JsonNode toJson() {
		return DerbiParser.ObjectToJson((T)this);
	}

	public List<T> read() {
		return DatabaseService.read((T)this);
	}

	public boolean delete() {
		return DatabaseService.delete((T)this);
	}

	public boolean update() {
		return DatabaseService.update((T)this);
	}

	public boolean create() {
		return DatabaseService.create((T)this);
	}

	@Override
	public List<T> read(JsonNode conditions) {
		return DatabaseService.read((T)this,conditions);
	}

	@Override
	public T getFrobDBByUniqueId() {
		return DatabaseService.getFrobDBByUniqueId((T)this);
	}

	@Override
	public boolean delete(JsonNode conditions) {
		return DatabaseService.delete((T)this,conditions);
	}

	@Override
	public boolean update(JsonNode conditions) {
		return DatabaseService.update((T)this,conditions);
	}
}
